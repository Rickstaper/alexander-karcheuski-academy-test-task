using System.IO;
public class Rover 
{
    /// <summary>
    /// FirstFuel, SecondFuel - fuel from point [0,0]
    /// </summary>
    static int FirstFuel { get; set; }
    static int SecondFuel { get; set; }
    /// <summary>
    /// ThirdFuel, FourthFuel - fuel from point [3,3]
    /// </summary>
    static int ThirdFuel { get; set; }
    static int FourthFuel { get; set; }
    /// <summary>
    /// The minimum amount of fuel out of 4 options
    /// </summary>
    static int MinimumFuel { get; set; }
    /// <summary>
    /// Amount of steps of the best way
    /// </summary>
    static int Steps { get; set; }
    /// <summary>
    /// Calculating the best way
    /// </summary>
    /// <param name="map">Matrix map</param>
    public static void CalculateRoverPath(int[,] map) 
    {
        string firstWay = BuildWay (map, 1);
        FirstFuel = CalculateFuel (map, firstWay);

        string secondWay = BuildWay (map, 2);
        SecondFuel = CalculateFuel (map, secondWay);

        string thirdWay = BuildWay (map, 3);
        ThirdFuel = CalculateFuel (map, thirdWay);

        string fourthWay = BuildWay (map, 4);
        FourthFuel = CalculateFuel (map, fourthWay);

        int[] fuels = { FirstFuel, SecondFuel, ThirdFuel, FourthFuel };
        string[] ways = { firstWay[..^2], secondWay[..^2], thirdWay[..^2], fourthWay[..^2] };

        int indexMinimumFuel;
        Steps = map.GetLength (0) + map.GetLength (1) - 2;
        FindTheBestWay (fuels, out indexMinimumFuel);
        WriteToFile (ways, indexMinimumFuel);
    }

    /// <summary>
    /// Build way
    /// </summary>
    /// <param name="map">Matrix map</param>
    /// <param name="numberWay">number way</param>
    /// <returns></returns>
    private static string BuildWay(int[,] map, int numberWay)
    {
        string way = string.Empty;
        switch(numberWay)
        {
            case 1:
                way = $"[0,0]->";
                for (int r = 0, c = 0; r < map.GetLength (0) && c < map.GetLength (1);)
                {
                    if (c == map.GetLength (1) - 1 && r == map.GetLength (0) - 1)
                    {
                        break;
                    }
                    if (r == map.GetLength (0) - 1)
                    {
                        way += $"[{r},{++c}]->";
                    }
                    else
                    {
                        if (c == map.GetLength (1) - 1)
                        {
                            way += $"[{++r},{c}]->";
                        }
                        else
                        {
                            way += Abs (map[r, c + 1] - map[r, c]) >= Abs (map[r + 1, c] - map[r,c]) ? $"[{++r},{c}]->" : $"[{r},{++c}]->";
                        }
                    }
                }
                break;
            case 2:
                way = $"[0,0]->";
                for (int r = 0, c = 0; r < map.GetLength (0) && c < map.GetLength (1);)
                {
                    if (c == map.GetLength (1) - 1 && r == map.GetLength (0) - 1)
                    {
                        break;
                    }
                    if (r == map.GetLength (0) - 1)
                    {
                        way += $"[{r},{++c}]->";
                    }
                    else
                    {
                        if (c == map.GetLength (1) - 1)
                        {
                            way += $"[{++r},{c}]->";
                        }
                        else
                        {
                            way += Abs (map[r, c + 1] - map[r, c]) <= Abs (map[r + 1, c] - map[r,c]) ? $"[{r},{++c}]->" : $"[{++r},{c}]->";
                        }
                    }
                }
                break;
            case 3:
                way = $"[{map.GetLength (0) - 1},{map.GetLength (1) - 1}]->";
                for (int r = map.GetLength (0) - 1, c = map.GetLength (1) - 1; r >= 0 && c >= 0;)
                {
                    if (c == 0 && r == 0)
                    {
                        break;
                    }
                    if (r == 0)
                    {
                        way += $"[{r},{--c}]->";
                    }
                    else
                    {
                        if (c == 0)
                        {
                            way += $"[{--r},{c}]->";
                        }
                        else
                        {
                            way += Abs (map[r, c - 1] - map[r, c]) >= Abs (map[r - 1, c] - map[r,c]) ? $"[{--r},{c}]->" : $"[{r},{--c}]->";
                        }
                    }
                }
                break;
            case 4:
                way = $"[{map.GetLength (0) - 1},{map.GetLength (1) - 1}]->";
                for (int r = map.GetLength (0) - 1, c = map.GetLength (1) - 1; r >= 0 && c >= 0;)
                {
                    if (c == 0 && r == 0)
                    {
                        break;
                    }
                    if (r == 0)
                    {
                        way += $"[{r},{--c}]->";
                    }
                    else
                    {
                        if (c == 0)
                        {
                            way += $"[{--r},{c}]->";
                        }
                        else
                        {
                            way += Abs (map[r, c - 1] - map[r, c]) <= Abs (map[r - 1, c] - map[r,c]) ? $"[{r},{--c}]->" : $"[{--r},{c}]->";
                        }
                    }
                }
                break;
        }

        return way;
    }

    /// <summary>
    /// taking a number modulo
    /// </summary>
    /// <param name="number">number</param>
    /// <returns></returns>
    private static int Abs(int number)
    {
        if (number < 0)
        {
            return number * -1;
        }

        return number;
    }

    /// <summary>
    /// Fuel calculate for each way
    /// </summary>
    /// <param name="map">matrix map</param>
    /// <param name="way">way</param>
    /// <returns></returns>
    private static int CalculateFuel(int[,] map, string way)
    {
        int fuel = 0;
        for (int i = 1; i < way.Length - 7; i+= 7)
        {
            fuel += Abs (map[way[i] - 48, way[i + 2] - 48] - map[way[i + 7] - 48, way[i + 9] - 48]) + 1;
        }

        return fuel;
    }

    /// <summary>
    /// Find the best way out of the proposed
    /// </summary>
    /// <param name="fuels">array of proposed fuel options</param>
    /// <param name="indexMinimumFuel">the best way index</param>
    private static void FindTheBestWay(int[] fuels, out int indexMinimumFuel)
    {
        MinimumFuel = FirstFuel;
        indexMinimumFuel = 0;

        for (int i = 1; i < 4; i++)
        {
            if (MinimumFuel > fuels[i])
            {
                MinimumFuel = fuels[i];
                indexMinimumFuel = i;
            }
        }
    }

    /// <summary>
    /// Write data to file at current directory
    /// </summary>
    /// <param name="ways"></param>
    /// <param name="indexMinimumFuel"></param>
    static void WriteToFile(string[] ways, int indexMinimumFuel)
    {
        string path = Path.Combine(System.Environment.CurrentDirectory, "path-plan.txt");

        try
        {
            using (StreamWriter file = new StreamWriter(path, false, System.Text.Encoding.Default))
            {
                file.WriteLine (ways[indexMinimumFuel]);
                file.WriteLine ($"steps:{Steps}");
                file.Write ($"fuel:{MinimumFuel}");
            }
        }
        catch(System.Exception e)
        {
            throw new System.Exception (nameof (CalculateRoverPath));
        }
    }
}
